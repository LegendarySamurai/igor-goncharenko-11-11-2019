import {
	GET_WEATHER_FORECAST,
	GET_FORECAST_BY_GEOPOSITION,
	INITIAL_WEATHER_REQUEST,
} from '../actions/types';

const INITIAL_FORECAST_STATE = {
	location: {
		EnglishName: '',
		key: ''
	},
	conditions: {
		text: '',
		EpochTime: null,
		temperatureMetric: { Value: 0, Unit: '' },
		temperatureImperial: { Value: 0, Unit: '' }
	},
	forecast: {}
};

export const weatherForecastReducer = (state = INITIAL_FORECAST_STATE, action) => {
	if (action.type === GET_FORECAST_BY_GEOPOSITION) {
		return action.payload;
	}
	else if (action.type === GET_WEATHER_FORECAST) {
		return action.payload;
	}
	else {
		return state;
	}
};

export const initialWeatherRequestReducer = (state = false, action) => {
	if (action.type === INITIAL_WEATHER_REQUEST) {
		return true;
	}
	else {
		return state;
	}
};
