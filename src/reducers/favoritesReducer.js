import {
	ADD_TO_FAVORITES,
	REMOVE_FROM_FAVORITES,
	GET_FAVORITES_FORECAST
} from '../actions/types';

export const manageFavoritesReducer = (state = [], action) => {
	if (action.type === ADD_TO_FAVORITES) {
		if (state.includes(action.payload)) {
			return state;
		}

		if (action.payload === null) {
			return state;
		}

		return [...state, action.payload];
	}
	else if (action.type === REMOVE_FROM_FAVORITES) {
		return [...state.filter(item => item !== action.payload)];
	}
	else {
		return state;
	}
};

export const favoritesForecastReducer = (state = [], action) => {
	if (action.type === GET_FAVORITES_FORECAST) {
		return [action.payload];
	}
	else {
		return state;
	}
};
