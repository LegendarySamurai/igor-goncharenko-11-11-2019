import { SHOW_ALERT, HIDE_ALERT } from '../actions/types';

export const handleAlertsReducer = (state = false, action) => {
	switch (action.type) {
		case SHOW_ALERT:
			return action.payload;
		case HIDE_ALERT:
			return false;
		default:
			return state;
	}
};
