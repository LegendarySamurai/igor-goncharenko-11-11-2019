import { combineReducers } from 'redux';
import {
	weatherForecastReducer,
	initialWeatherRequestReducer,
} from './forecastReducer';
import {
	manageFavoritesReducer,
	favoritesForecastReducer
} from './favoritesReducer'
import { handleAlertsReducer } from './alertsReducer';
import { settingsReducer } from './settingsReducer';

export default combineReducers({
	forecast: weatherForecastReducer,
	favorites: manageFavoritesReducer,
	favoritesForecast: favoritesForecastReducer,
	initialWeatherRequest: initialWeatherRequestReducer,
	alert: handleAlertsReducer,
	settings: settingsReducer
});
