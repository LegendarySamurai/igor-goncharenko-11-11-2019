import { CHANGE_THEME, CHANGE_UNITS } from '../actions/types';

export const settingsReducer = (state = {}, action) => {
	switch (action.type) {
		case CHANGE_THEME:
			return { ...state, ...{ theme: action.payload } };
		case CHANGE_UNITS:
			return { ...state, ...{ units: action.payload } };
		default:
			return state;
	}
};
