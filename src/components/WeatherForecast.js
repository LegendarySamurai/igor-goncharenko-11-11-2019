import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Col, Container, Row, Spinner } from 'react-bootstrap';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import AddToFavorites from './AddToFavorites';
import DailyForecast from './DailyForecast';
import { getForecastByGeoposition, changeInitialState } from '../actions'

library.add(faCheck);

class WeatherForecast extends Component {
	componentDidMount () {
		if (!this.props.initialWeatherRequest) {
			this.props.getForecastByGeoposition();

			this.props.changeInitialState();
		}
	}

	isInFavorites = (locationKey) => this.props.favorites.includes(locationKey);

	renderForecast = () => {
		if (this.props.forecast.forecast.length > 0) {
			return this.props.forecast.forecast.map((dailyForecast) => (
				<DailyForecast { ...dailyForecast } key={ dailyForecast.EpochDate }/>
			));
		}
	};

	render () {
		const {
			      location: { location, key: locationKey },
			      conditions: {
				      text: conditionsText,
				      temperatureMetric: { Value: metricValue, Unit: metricUnit },
				      temperatureImperial: { Value: imperialValue, Unit: imperialUnit }
			      }
		      } = this.props.forecast;

		return (
			<section className="weather-forecast">
				<Container>
					<Row>
						<Col xs={ 12 }>
							<div className="weather-forecast--content">


								{
									!this.props.forecast.location.location && (
										<div className="spinner">
											<Spinner animation="border" role="status">
												<span className="sr-only">Loading...</span>
											</Spinner>
										</div>
									)
								}

								<header className="weather-forecast--content-header">
									<Row className="justify-content-lg-between">
										<Col xs={ 6 } lg="auto">
											<div className="location">
												<h1 className="location__title">
													{ location }
													<span
														className={`favorites-status ${ this.isInFavorites(locationKey) ? 'is-active' : null }`}
														title="In Favorites"
													>
														<FontAwesomeIcon icon={ faCheck }/>
													</span>
												</h1>
												<p className="location__conditions">
													{
														`
														${ this.props.units ? imperialValue.toFixed(0)
																: metricValue.toFixed(0) }
														${ this.props.units ? imperialUnit : metricUnit }
														`
													}
												</p>
											</div>
										</Col>
										<Col xs="auto" className="ml-auto ml-lg-0">
											<AddToFavorites locationKey={ locationKey }/>
										</Col>
									</Row>
								</header>

								<div className="weather-forecast--content-body">
									<div className="weather-forecast__conditions-text">
										{ conditionsText }
									</div>
									<Row className="daily-forecast-container">
										{ this.renderForecast() }
									</Row>
								</div>
							</div>
						</Col>
					</Row>
				</Container>
			</section>
		);
	}
};

const mapStateToProps = (state) => ({
	favorites: state.favorites,
	forecast: state.forecast,
	defaultForecast: state.defaultForecast,
	initialWeatherRequest: state.initialWeatherRequest,
	units: state.settings.units
});


export default connect(mapStateToProps, { getForecastByGeoposition, changeInitialState })(WeatherForecast);
