import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Container, Row } from 'react-bootstrap';
import { getFavoritesForecast } from '../actions';
import FavoritesForecast from './FavoritesForecast';

class Favorites extends Component {
	componentDidMount () {
		this.props.favorites.forEach((item) => {
			this.props.dispatch(getFavoritesForecast(item));
		});
	}

	renderWeather () {
		return this.props.favoritesForecast.map((location) =>
			<FavoritesForecast { ...location } key={ location.location.key }/>);
	}

	render () {
		return (
			<main>
				<Container>
					<Row>
						{ this.renderWeather() }
					</Row>
				</Container>
			</main>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		favorites: state.favorites,
		favoritesForecast: state.favoritesForecast
	}
};

export default connect(mapStateToProps)(Favorites);
