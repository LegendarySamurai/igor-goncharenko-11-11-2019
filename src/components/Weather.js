import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Alert } from 'react-bootstrap';
import Search from './Search';
import WeatherForecast from './WeatherForecast';
import { hideAlert } from '../actions';

class Weather extends Component {
	render () {
		return (
			<main>
				{
					this.props.alert === 'invalid location' && (
						<Alert
							className="alert__invalid-location"
							variant="danger"
							onClose={ () => { this.props.hideAlert() } }
							dismissible>
							<Alert.Heading>Oops...</Alert.Heading>
							<p>Something went wrong :(</p>
						</Alert>
					)
				}
				<Search/>
				<WeatherForecast/>
			</main>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		alert: state.alert
	}
};

export default connect(mapStateToProps, { hideAlert })(Weather);
