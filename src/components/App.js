import React from 'react';
import { connect } from 'react-redux';
import { Router, Route } from 'react-router-dom';

import history from '../history';
import Header from './Header';
import Weather from './Weather';
import Favorites from './Favorites';

const App = (props) => {
	return (
		<div className={ props.theme ? 'theme-dark app' : 'theme-light app' }>
			<Router history={ history }>
				<Header/>
				<Route path="/" exact component={ Weather }/>
				<Route path="/favorites" component={ Favorites }/>
			</Router>
		</div>
	);
};

const mapStateToProps = (state) => {
	return {
		theme: state.settings.theme
	}
};

export default connect(mapStateToProps)(App);
