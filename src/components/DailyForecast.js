import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { Col, Card } from 'react-bootstrap';
import { convertCelsiusToFahrenheit } from '../utils/tempConverter';

const DailyForecast = (props) => {

	const {
		      EpochDate,
		      Temperature: {
			      Minimum: { Value: minValue },
			      Maximum: { Value: maxValue }
		      }
	      } = props;

	const forecast = {
		dayOfWeek: moment(EpochDate * 1000).format('ddd'),
		averageDailyTemp: ((minValue + maxValue) / 2).toFixed(0),
	};

	const { dayOfWeek, averageDailyTemp } = forecast;

	return (
		<Col xs={ 12 } md={ 4 } lg="auto" className="daily-forecast-unit">
			<Card>
				<Card.Body>
					<Card.Title>{ dayOfWeek }</Card.Title>
					<Card.Text>
						{
							props.units ?
								`${ convertCelsiusToFahrenheit(averageDailyTemp) } F`
								: `${ averageDailyTemp } C`
						}
					</Card.Text>
				</Card.Body>
			</Card>
		</Col>
	);
};

const mapStateToProps = (state) => {
	return {
		units: state.settings.units
	}
};

export default connect(mapStateToProps)(DailyForecast);
