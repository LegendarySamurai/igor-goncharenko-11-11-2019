import React from 'react';
import { connect } from 'react-redux';
import history from '../history';
import { Col, Card } from 'react-bootstrap';
import { getWeatherForecast } from '../actions';

const FavoritesForecast = (props) => {
	const {
		      location: { location, key },
		      conditions: {
			      text: conditionsText,
			      temperatureMetric: { Value: metricValue, Unit: metricUnit },
			      temperatureImperial: { Value: imperialValue, Unit: imperialUnit },
		      }
	      } = props;

	const handleRedirect = () => {
		props.getWeatherForecast(location);
		history.push('/');
	};

	return (
		<Col xs={ 12 } md={ 4 } lg="auto" className="daily-forecast-unit">
			<Card onClick={ () => { handleRedirect(key) } }>
				<Card.Body>
					<Card.Title>{ location }</Card.Title>
					<Card.Text>
						{ `
							${
							props.units ?
								imperialValue.toFixed(0)
								: metricValue.toFixed(0)
						}
							${ props.units ? imperialUnit	: metricUnit }
						` }
					</Card.Text>
					<Card.Text>{ conditionsText }</Card.Text>
				</Card.Body>
			</Card>
		</Col>
	);
};

const mapStateToProps = (state) => {
	return {
		units: state.settings.units
	}
};

export default connect(mapStateToProps, { getWeatherForecast })(FavoritesForecast);
