import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Container, Row, Col, Navbar, Nav, Button, Form } from 'react-bootstrap';
import { changeTheme, changeUnits } from '../actions';

const Header = (props) => {
	return (
		<header className="page-header">
			<Container fluid={ true }>
				<Row>
					<Col xs={ 12 } className="px-0">
						<Navbar
							variant={ props.theme ? 'dark' : 'light' }
							expand="md"
						>
							<Navbar.Brand as="div">
								<Link to="/" className="nav-link">Herolo Weather App</Link>
							</Navbar.Brand>
							<Navbar.Toggle aria-controls="basic-navbar-nav"/>
							<Navbar.Collapse id="basic-navbar-nav">
								<Nav className="ml-auto">
									<Link to="/" className="nav-link">
										<Button variant="light">Home</Button>
									</Link>
									<Link to="/favorites" className="nav-link">
										<Button variant="light">Favorites</Button>
									</Link>
								</Nav>
							</Navbar.Collapse>
						</Navbar>
					</Col>
				</Row>
				<Row className={ `py-3 border-top ${ props.theme ? 'border-light' : null }` }>
					<Col xs={ 6 } lg="auto" className="d-flex justify-content-center ml-lg-auto">
						<Form.Check
							type="switch"
							id="switch-units"
							className="switch-units"
							label={ props.units ? 'Fahrenheit' : 'Celsius' }
							onChange={ (e) => { props.changeUnits(e.target.checked) } }
							checked={ props.units ? 'checked' : null }
						/>
					</Col>
					<Col xs={ 6 } lg="auto" className="d-flex justify-content-center ml-lg-2">
						<Form.Check
							type="switch"
							id="switch-theme"
							className="switch-theme"
							label={ props.theme ? 'dark' : 'light' }
							onChange={ (e) => { props.changeTheme(e.target.checked) } }
							checked={ props.theme ? 'checked' : null }
						/>
					</Col>
				</Row>
			</Container>
		</header>
	)
};

const mapStateToProps = (state) => {
	return {
		theme: state.settings.theme,
		units: state.settings.units
	}
};

export default connect(
	mapStateToProps,
	{ changeTheme, changeUnits }
)(Header);
