import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Container, Row, Col, Form, InputGroup } from 'react-bootstrap';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { getWeatherForecast } from '../actions';

library.add(faSearch);

class Search extends Component {
	state = {
		searchInput: ''
	};

	handleFormSubmit = (e) => {
		e.preventDefault();
		this.props.getWeatherForecast(this.state.searchInput);
		this.setState({ searchInput: '' });
	};

	render () {
		return (
			<section className="search py-lg-3">
				<Container>
					<Row>
						<Col lg={ 6 } className="mx-auto">
							<Form onSubmit={ this.handleFormSubmit }>
								<Form.Row>
									<Col xs>
										<Form.Group className="mb-0">
											<InputGroup>
												<InputGroup.Prepend>
													<InputGroup.Text id="inputGroupPrepend">
														<FontAwesomeIcon
															icon={ faSearch }
															style={ { 'color': '#999' } }
														/>
													</InputGroup.Text>
												</InputGroup.Prepend>
												<Form.Control
													type="text"
													placeholder="City name"
													name="city"
													value={ this.state.searchInput }
													onChange={ (e) => { this.setState({ searchInput: e.target.value }) } }
												/>
											</InputGroup>
										</Form.Group>
									</Col>
								</Form.Row>
							</Form>
						</Col>
					</Row>
				</Container>
			</section>
		);
	}
}

export default connect(null, { getWeatherForecast })(Search);
