import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button } from 'react-bootstrap';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faHeart, faMinusCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { addToFavorites, removeFromFavorites } from '../actions';

library.add(faHeart, faMinusCircle);

class AddToFavorites extends Component {
	isInFavorites = (locationKey) => this.props.favorites.includes(locationKey);

	handleFavoritesManagement (locationKey) {
		if (this.isInFavorites(locationKey)) {
			this.props.removeFromFavorites(locationKey);
		}
		else if (!this.isInFavorites(locationKey) && locationKey) {
			this.props.addToFavorites(locationKey);
		}
	}

	render () {
		const { key: locationKey } = this.props.forecast.location;

		return (
			<Button
				variant={ this.isInFavorites(locationKey) ? 'outline-danger' : 'info' }
				className="btn-add-to-favorites"
				onClick={ () => { this.handleFavoritesManagement(locationKey) } }>
				<FontAwesomeIcon
					icon={ this.isInFavorites(locationKey) ? faMinusCircle : faHeart }
					className="mr-lg-2"
				/>
				<span>
					{
						this.isInFavorites(locationKey) ?
							'Remove from Favorites'
							: 'Add to Favorites'
					}
				</span>
			</Button>
		)
	}
};

const mapStateToProps = (state) => {
	return {
		favorites: state.favorites,
		forecast: state.forecast
	}
};

export default connect(
	mapStateToProps,
	{ addToFavorites, removeFromFavorites }
)(AddToFavorites);
