import accuweather from '../apis/accuweather';
import {
	GET_WEATHER_FORECAST,
	GET_LOCATION,
	GET_LOCATION_BY_KEY,
	GET_CONDITIONS,
	GET_FORECAST,
	ADD_TO_FAVORITES,
	REMOVE_FROM_FAVORITES,
	GET_FAVORITES_FORECAST,
	GET_FORECAST_BY_GEOPOSITION,
	INITIAL_WEATHER_REQUEST,
	CHANGE_THEME,
	CHANGE_UNITS,
	SHOW_ALERT,
	HIDE_ALERT
} from './types';

const apikey = 'v3gsBElkvI5rI9wKmokK8LmGfCglu6fm';

export const getWeatherForecast = (location) => async (dispatch) => {
	try {
		const locationKey = await dispatch(getLocation(location));
		const conditions = await dispatch(getConditions(locationKey.key));
		const forecast = await dispatch(getForecast(locationKey.key));

		await dispatch({
			type: GET_WEATHER_FORECAST,
			payload: {
				location: locationKey,
				conditions: conditions,
				forecast: forecast,
			}
		});
	}
	catch (err) {
		dispatch(showAlert('invalid location'));
	}
};

export const getLocation = (request) => async (dispatch) => {
	const response = await accuweather.get('locations/v1/cities/autocomplete', {
			params: {
				apikey,
				q: request
			}
		}
	);

	const { Key: key, LocalizedName: location } = response.data[0];

	dispatch({
		type: GET_LOCATION,
		payload: {
			key,
			location
		}
	});

	return { key, location };
};

export const getLocationByKey = (key) => async (dispatch) => {
	const response = await accuweather.get(`locations/v1/${ key }`, {
			params: {
				apikey
			}
		}
	);

	dispatch({
		type: GET_LOCATION_BY_KEY,
		payload: {
			location: response.data.EnglishName,
			key
		}
	});

	return { location: response.data.EnglishName, key };
};

export const getConditions = (locationKey) => async (dispatch) => {
	const response = await accuweather.get(`/currentconditions/v1/${ locationKey }`, {
			params: { apikey }
		}
	);

	const { EpochTime, WeatherText, Temperature: { Metric, Imperial } } = response.data[0];

	const currentConditions = {
		EpochTime,
		text: WeatherText,
		temperatureMetric: Metric,
		temperatureImperial: Imperial,
	};

	dispatch({
		type: GET_CONDITIONS,
		payload: currentConditions
	});

	return currentConditions;
};

export const getForecast = (locationKey) => async (dispatch) => {
	const response = await accuweather.get(`/forecasts/v1/daily/5day/${ locationKey }`, {
			params: {
				apikey,
				metric: true
			}
		}
	);

	dispatch({
		type: GET_FORECAST,
		payload: response.data.DailyForecasts
	});

	return response.data.DailyForecasts;
};

export const addToFavorites = (location) => {
	return { type: ADD_TO_FAVORITES, payload: location };
};

export const removeFromFavorites = (location) => {
	return { type: REMOVE_FROM_FAVORITES, payload: location }
};

export const getFavoritesForecast = (key) => async (dispatch) => {
	const location = await dispatch(getLocationByKey(key));
	const conditions = await dispatch(getConditions(key));

	await dispatch({
		type: GET_FAVORITES_FORECAST,
		payload: { location, conditions }
	});
};

export const getForecastByGeoposition = () => async (dispatch) => {
	let currentGeoPosition;

	try {
		const geoPosition = await new Promise((resolve, reject) => {
			navigator.geolocation.getCurrentPosition(resolve, reject);
		});

		currentGeoPosition = {
			latitude: geoPosition.coords.latitude,
			longitude: geoPosition.coords.longitude
		}

		const { latitude, longitude } = currentGeoPosition;

		const response = await accuweather.get(`/locations/v1/cities/geoposition/search`, {
			params: {
				apikey,
				q: `${ latitude },${ longitude }`
			}
		});


		const location = await dispatch(getLocationByKey(response.data.Key));
		const conditions = await dispatch(getConditions(response.data.Key));
		const forecast = await dispatch(getForecast(response.data.Key));

		await dispatch({
			type: GET_FORECAST_BY_GEOPOSITION,
			payload: {
				location: location,
				conditions: conditions,
				forecast: forecast,
			}
		});
	}
	catch (error) {
		dispatch(showAlert('invalid location'));
	}
};

export const changeInitialState = () => {
	return { type: INITIAL_WEATHER_REQUEST }
};

export const changeTheme = (value) => {
	return { type: CHANGE_THEME, payload: value }
};

export const changeUnits = (value) => {
	return { type: CHANGE_UNITS, payload: value }
};

export const showAlert = (alertType) => {
	return { type: SHOW_ALERT, payload: alertType	}
};

export const hideAlert = () => {
	return { type: HIDE_ALERT	}
};
