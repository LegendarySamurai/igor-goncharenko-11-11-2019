export const convertFahrenheitToCelsius = (tempFahrenheit) => {
	return ((tempFahrenheit - 32) * 5 / 9).toFixed(0);
};

export const convertCelsiusToFahrenheit = (tempCelsius) => {
	return (tempCelsius * 9/5 + 32).toFixed(0);
};
