import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import reduxThunk from 'redux-thunk';

import reducers from './reducers'
import App from './components/App';
import { loadState, saveState } from './utils/localStorage'

import './styles/app.scss';


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistedState = loadState();
const store = createStore(
	reducers,
	persistedState,
	composeEnhancers(applyMiddleware(reduxThunk))
);

store.subscribe(() => {
	saveState({
		favorites: store.getState().favorites,
		settings: store.getState().settings
	});
});

ReactDOM.render(
	<Provider store={ store }>
		<App/>
	</Provider>,
	document.querySelector('#root')
);
